function obtenerSaludo() {
    const fecha = new Date()
    const hora = fecha.getHours()

    if (hora < 12 && hora >= 5) {
        return "Buenos dias"
    }

    if (hora < 17) {
        return "Buenas tardes"
    }

    if (hora < 24 || hora < 5) {
        return "Buenas noches"
    }
}

console.log(obtenerSaludo())
